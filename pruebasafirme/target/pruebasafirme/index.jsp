<%@page import="org.jsoup.safety.Whitelist"%>
<%@page import="org.jsoup.Jsoup"%>
<%@page import="org.jsoup.nodes.Document"%>
<%@page import="org.owasp.encoder.Encode"%>
<%
	
%>
<%
	Document doc; 
	String action = "respuesta.jsp";
	String str = Jsoup.clean("f1", Whitelist.basic());
	doc = Jsoup.parse("f1");
%>
<html> 
<head>
<style type="text/css">
body {
	background: #fff;
}
#<%=doc%> {
	background: purple;
}
</style>
<title>Java encoder</title>
</head>
<body>
	
	<form id="<%=doc%>" 
		action="<%= Encode.forHtmlAttribute(action) %>"
		method="<%= Encode.forHtmlAttribute("post") %>">
	
		Nombre <input type="<%= Encode.forHtmlAttribute("text") %>" 
		name="<%= Encode.forHtmlAttribute("nombre") %>">
		
		<button type="submit" value="enviar">Enviar</button>
	</form>

</body>
</html>